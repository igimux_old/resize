class Services::FileCompression
  attr_accessor :error, :images

  DEFAULT_OPTIONS = {
    iteration: 3,
    max_width: 3000,
    max_height: 2000,
    size: 300000,
    jpg_quality: [75, 50, 25],
    png_compress_level: [1, 3, 9]
  }

  def initialize(file_path, locals: {}, is_folder: false)
    @file_path = file_path.to_s
    @options   = DEFAULT_OPTIONS
    @is_folder = is_folder
    @images    = []

    DEFAULT_OPTIONS.keys.each do |key|
      @options[key] = locals[key] if locals[key].present?
    end
  end

  def call
    get_images
    do_compression
  rescue => e
    @error = e.message
  end

  private

  def get_images
    @is_folder ? @images = Dir.glob("#{@file_path}/*.{png,jpg,jpeg}") : @images << @file_path
  end

  def do_compression
    @images.each do |image|
      filename = File.basename(image)
      image = MiniMagick::Image.open(image)

      next unless %w(JPEG JPG PNG).include?(image.type)
      next unless %w(image/jpeg image/jpg image/png).include?(image.mime_type)

      next if size_normal?(image)

      image_path = @is_folder ? "#{@file_path}/#{filename}" : @file_path
      image = resize_image(image)
      image.write image_path

      next if size_normal?(image)

      @options[:iteration].times do |i|
        image_tmp = compress_quality(image, i)

        if size_normal?(image_tmp) || i + 1 == @options[:iteration]
          image_tmp.write image_path
          break
        end
      end
    end
  end

  def size_normal?(image)
    image.size <= @options[:size]
  end

  def resize_image(image)
    image.combine_options do |b|
      b.resize "#{@options[:max_width]}x#{@options[:max_height]}>"
    end

    image
  end

  def compress_quality(image, i)
    if image.type == 'PNG'
      MiniMagick::Tool::Convert.new do |convert|
        convert << image.path
        convert << "-strip"
        convert << "-define"
        convert << "png:compression-filter=5"
        convert << "-define"
        convert << "png:compression-level=#{@options[:png_compress_level][i]}"
        convert << "-define"
        convert << "png:compression-strategy=1"
        convert << image.path + "_tmp.png"
      end

      image = MiniMagick::Image.open(image.path + "_tmp.png")
    elsif ['JPEG', 'JPG'].include?(image.type)
      image.quality(@options[:jpg_quality][i])
    end

    image
  end
end