class IndexController < ApplicationController
  def view

  end

  def update

    #загружаем logo
    if params[:spot][:picture]
      uploaded_io = params[:spot][:picture]
      ext=(uploaded_io.original_filename).split('.')[-1]
      type=['gif','jpg','jpeg','png','svg','svgz']
      if type.nil? || type.include?(ext)
        file_name ="#{@spot.nasid}.#{ext}"
      else
        redirect_to edit_spot_path(@spot.nasid)
        flash.notice = 'Неверный тип файла'
        return
      end

      File.open(Rails.root.join('public', 'files', 'uploads',  file_name), 'wb') do |file|
        file.write(uploaded_io.read)
      end




      file_path = Rails.root.join('public', 'files', 'uploads',  file_name)

      file_compression_service = Services::FileCompression.new(file_path)
      file_compression_service.call

      if file_compression_service.error.present?
        #TODO обработка ошибки
      end





      @spot.logo1=file_name
      adder = ' Изображение загружено.'
    end


    #загружаем фон
    if params[:spot][:bgpicture]
      uploaded_io = params[:spot][:bgpicture]
      ext=(uploaded_io.original_filename).split('.')[-1]
      type=['gif','jpg','jpeg','png','svg','svgz']
      if type.nil? || type.include?(ext)
        file_name ="bg#{@spot.nasid}.#{ext}"
      else
        redirect_to edit_spot_path(@spot.nasid)
        flash.notice = 'Неверный тип файла'
        return
      end
      File.open(Rails.root.join('public', 'files', 'uploads',  file_name), 'wb') do |file|
        file.write(uploaded_io.read)
      end
      params[:spot][:bg]=file_name
      adder = ' Изображение загружено.'
    end

    #загружаем баннер
    if params[:spot][:banner]
      uploaded_io = params[:spot][:banner]
      ext=(uploaded_io.original_filename).split('.')[-1]
      type=['gif','jpg','jpeg','png']
      if type.nil? || type.include?(ext)
        file_name ="bnr#{@spot.nasid}.#{ext}"
      else
        redirect_to edit_spot_path(@spot.nasid)
        flash.notice = 'Неверный тип файла'
        return
      end

      File.open(Rails.root.join('public', 'files', 'uploads',  file_name), 'wb') do |file|
        file.write(uploaded_io.read)
      end
      @spot.banner=file_name
      adder = ' Изображение загружено.'
    end

    respond_to do |format|
      if @spot.update(spot_params)
        format.html { redirect_to edit_spot_path(@spot.nasid)+"?tab="+params['tab'].to_s, notice: 'Информация о хотспоте была обновлена.'+adder.to_s }
        format.json { render :show, status: :ok, location: @spot }
      else
        format.html { render :edit }
        format.json { render json: @spot.errors, status: :unprocessable_entity }
      end
    end

  end
end
