# README


**Вызов микросервиса уменьшения всех изображений в директории** 

`compression_service = Services::FileCompression.new(dir_path, is_folder: true)`

`compression_service.call`

Пример dir_path: `/var/www/project/public/images`

**Вызов микросервиса уменьшения конкретного изображения** 

`compression_service = Services::FileCompression.new(file_path)`

`compression_service.call`

Пример file_path: `/var/www/project/public/images/first.jpg`

**Обработка ошибки при выполнении микросервиса**

`// TODO... if compression_service.error.present?`

**Путь до файла класса микросервиса:** `app/core/services/file_compression.rb`