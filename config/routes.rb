Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'index#view'
  post 'update' => 'index#update', as: :index_update
end
